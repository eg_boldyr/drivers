CREATE SCHEMA opti;

CREATE TABLE opti.drivers (
    id INT(11) AUTO_INCREMENT,
    phone BIGINT(12) NOT NULL,
    name NVARCHAR(50) NOT NULL,
    car_model NVARCHAR(50) NOT NULL,
    car_number NVARCHAR(50) NOT NULL,
    car_color NVARCHAR(50) NOT NULL,
    car_year YEAR NOT NULL,
    dt TIMESTAMP,
    active INT(1),
    PRIMARY KEY (id)
);

COMMIT WORK;

CREATE TABLE opti.hibernate_sequence (
    next_val INT(11)
);
INSERT INTO opti.hibernate_sequence VALUES (1);

COMMIT WORK;

