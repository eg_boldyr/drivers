package com.drivers.execption;

public class DriverDataException extends RuntimeException {

    private String errorCode;

    public DriverDataException(String errorCode, String message) {
        super(message);
        this.errorCode = errorCode;
    }

    public String getErrorCode() {
        return errorCode;
    }
}
