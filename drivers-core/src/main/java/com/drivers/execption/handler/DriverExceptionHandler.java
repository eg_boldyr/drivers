package com.drivers.execption.handler;

import com.drivers.api.response.GeneralResponse;
import com.drivers.api.response.ResponseCodes;
import com.drivers.execption.DriverDataException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.util.WebUtils;

@ControllerAdvice
public class DriverExceptionHandler {

    @ExceptionHandler(value = DriverDataException.class)
    protected ResponseEntity<Object> handleManagementDataException(DriverDataException exc, WebRequest request) {
        return handleException(exc, new GeneralResponse<String>(ResponseCodes.ERROR, exc.getErrorCode(), exc.getMessage()), request);
    }

    private ResponseEntity<Object> handleException(Exception exc, Object body, WebRequest request) {
        request.setAttribute(WebUtils.ERROR_EXCEPTION_ATTRIBUTE, exc, WebRequest.SCOPE_REQUEST);
        return new ResponseEntity<Object>(body, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
