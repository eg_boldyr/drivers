package com.drivers.endpoint;

import com.drivers.api.endpoint.DriversEndpoint;
import com.drivers.api.request.GeneralRequest;
import com.drivers.api.response.GeneralResponse;
import com.drivers.api.response.ResponseCodes;
import com.drivers.api.response.parameters.DriverIDParam;
import com.drivers.api.response.parameters.DriverParams;
import com.drivers.service.IDriverService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Validated
@RestController
public class DriversRestEndpoint implements DriversEndpoint {

    private final IDriverService driverService;

    @Autowired
    public DriversRestEndpoint(IDriverService driverService) {
        this.driverService = driverService;
    }

    @Override
    public GeneralResponse<Integer> createDriverCard(@RequestBody @Valid @NotNull GeneralRequest<DriverParams> request) {
        return new GeneralResponse<Integer>(ResponseCodes.OK, "Success", driverService.create(request.getParameters()));
    }

    @Override
    public GeneralResponse<Void> statusDriverCard(
            @PathVariable("option") @NotBlank @Pattern(regexp = "activate|deactivate") String option,
            @RequestBody @Valid @NotNull GeneralRequest<DriverIDParam> request) {
        driverService.status(option, request.getParameters());
        return new GeneralResponse<>(ResponseCodes.OK, "Success", null);
    }
}