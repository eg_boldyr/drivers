package com.drivers.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Calendar;
import java.util.Date;
import java.util.StringJoiner;

@Entity
@Table(name = "drivers")
public class Driver {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @Column(columnDefinition = "BIGINT(12)")
    private Long phone;
    @Column(columnDefinition = "VARCHAR(50)")
    private String name;
    @Column(name = "car_model", columnDefinition = "VARCHAR(50)")
    private String carModel;
    @Column(name = "car_number", columnDefinition = "VARCHAR(50)")
    private String carNumber;
    @Column(name = "car_color", columnDefinition = "VARCHAR(50)")
    private String carColor;
    @Column(name = "car_year", columnDefinition = "YEAR")
    private Integer carYear;
    @Temporal(TemporalType.TIMESTAMP)
    private Date dt;
    @Column(columnDefinition = "INT(1)")
    private Integer active;

    public Driver() {
        this.dt = Calendar.getInstance().getTime();
        this.active = 0;
    }

    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public Long getPhone() {
        return phone;
    }
    public void setPhone(Long phone) {
        this.phone = phone;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getCarModel() {
        return carModel;
    }
    public void setCarModel(String carModel) {
        this.carModel = carModel;
    }
    public String getCarNumber() {
        return carNumber;
    }
    public void setCarNumber(String carNumber) {
        this.carNumber = carNumber;
    }
    public String getCarColor() {
        return carColor;
    }
    public void setCarColor(String carColor) {
        this.carColor = carColor;
    }
    public Integer getCarYear() {
        return carYear;
    }
    public void setCarYear(Integer carYear) {
        this.carYear = carYear;
    }
    public Date getDt() {
        return dt;
    }
    public Integer getActive() {
        return active;
    }
    public void setActive(Integer active) {
        this.active = active;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Driver.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("phone=" + phone)
                .add("name='" + name + "'")
                .add("carModel='" + carModel + "'")
                .add("carNumber='" + carNumber + "'")
                .add("carColor='" + carColor + "'")
                .add("carYear=" + carYear)
                .add("dt=" + dt)
                .add("active=" + active)
                .toString();
    }
}
