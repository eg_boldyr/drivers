package com.drivers.util;

public final class ErrorCode {

    public static final String DRIVER_NOT_FOUND         = "driver.record.not.found";
    public static final String DRIVER_NOT_FOUND_MESSAGE = "Карта водителя с [ID: %d] не найдена.";

}
