package com.drivers.util;

public final class Constants {

    public static final String ACTIVATE_OPTION = "activate";
    public static final String DEACTIVATE_OPTION = "deactivate";

}
