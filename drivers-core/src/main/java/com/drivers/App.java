package com.drivers;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;

@SpringBootApplication
@PropertySource({"file:config/application.properties", "file:config/db.properties"})
public class App {

    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }
}
