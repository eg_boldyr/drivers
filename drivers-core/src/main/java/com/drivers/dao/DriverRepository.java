package com.drivers.dao;

import com.drivers.domain.Driver;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DriverRepository extends JpaRepository<Driver, Integer> {

    Driver findByIdEquals(Integer id);

}
