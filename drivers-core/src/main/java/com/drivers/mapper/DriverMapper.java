package com.drivers.mapper;

import com.drivers.api.response.parameters.DriverParams;
import com.drivers.domain.Driver;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring")
public interface DriverMapper {

    @Mappings({
            @Mapping(target = "carModel", source = "car.model"),
            @Mapping(target = "carNumber", source = "car.number"),
            @Mapping(target = "carColor", source = "car.color"),
            @Mapping(target = "carYear", source = "car.year")
    })
    Driver driverParamsToDriverDomain(DriverParams params);
}
