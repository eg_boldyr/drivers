package com.drivers.service;

import com.drivers.api.response.parameters.DriverIDParam;
import com.drivers.api.response.parameters.DriverParams;
import com.drivers.dao.DriverRepository;
import com.drivers.domain.Driver;
import com.drivers.execption.DriverDataException;
import com.drivers.mapper.DriverMapper;
import com.drivers.util.Constants;
import com.drivers.util.ErrorCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Objects;

@Service
@Transactional
public class DriverService implements IDriverService {

    private DriverRepository driverRepository;
    private DriverMapper driverMapper;

    @Autowired
    public void setDriverRepository(DriverRepository driverRepository) {
        this.driverRepository = driverRepository;
    }
    @Autowired
    public void setDriverMapper(DriverMapper driverMapper) {
        this.driverMapper = driverMapper;
    }

    @Override
    public Integer create(DriverParams params) {
        return driverRepository
                .saveAndFlush(driverMapper.driverParamsToDriverDomain(params))
                .getId();
    }

    @Override
    public void status(String option, DriverIDParam param) {
        Driver driver = driverRepository.findByIdEquals(param.getDriverId());

        if (Objects.isNull(driver)) {
            throw new DriverDataException(ErrorCode.DRIVER_NOT_FOUND,
                    String.format(ErrorCode.DRIVER_NOT_FOUND_MESSAGE, param.getDriverId()));
        }

        switch (option) {
            case Constants.ACTIVATE_OPTION : {
                driver.setActive(1);
                break;
            }
            case Constants.DEACTIVATE_OPTION : {
                driver.setActive(0);
            }
        }

        driverRepository.saveAndFlush(driver);
    }
}
