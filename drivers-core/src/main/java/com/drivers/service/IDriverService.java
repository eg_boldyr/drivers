package com.drivers.service;

import com.drivers.api.response.parameters.DriverIDParam;
import com.drivers.api.response.parameters.DriverParams;

public interface IDriverService {

    Integer create(DriverParams params);

    void status(String option, DriverIDParam param);
}
