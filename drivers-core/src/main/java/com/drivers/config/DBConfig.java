package com.drivers.config;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaDialect;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

@Configuration
@EnableJpaRepositories("com.drivers.dao")
@EnableTransactionManagement
public class DBConfig {

    @Bean
    @ConfigurationProperties("c3p0.named-configs.driver")
    public DataSource driversDataSource() {
        return new ComboPooledDataSource();
    }

    @Bean(name = "entityManagerFactory")
    public LocalContainerEntityManagerFactoryBean driversEntityManager(DataSource driversDataSource, JpaVendorAdapter jpaVendorAdapter) {
        LocalContainerEntityManagerFactoryBean entityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();
        entityManagerFactoryBean.setDataSource(driversDataSource);
        entityManagerFactoryBean.setJpaDialect(new HibernateJpaDialect());
        entityManagerFactoryBean.setJpaVendorAdapter(jpaVendorAdapter);
        entityManagerFactoryBean.setPersistenceUnitName("drivers-DB");
        entityManagerFactoryBean.setPackagesToScan("com.drivers.domain");
        return entityManagerFactoryBean;
    }

    @Bean(name = "transactionManager")
    public PlatformTransactionManager driversTransactionManager(
            @Qualifier("entityManagerFactory") EntityManagerFactory driversEntityManager) {
        return new JpaTransactionManager(driversEntityManager);
    }

    @Bean
    public JpaVendorAdapter jpaVendorAdapter(@Value("${hibernate.dialect}") String dialect) {
        HibernateJpaVendorAdapter jpaVendorAdapter = new HibernateJpaVendorAdapter();
        jpaVendorAdapter.setDatabasePlatform(dialect);
        return jpaVendorAdapter;
    }
}
