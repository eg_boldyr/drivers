package com.drivers.api.endpoint;

import com.drivers.api.request.GeneralRequest;
import com.drivers.api.response.GeneralResponse;
import com.drivers.api.response.parameters.DriverIDParam;
import com.drivers.api.response.parameters.DriverParams;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Info;
import io.swagger.annotations.SwaggerDefinition;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@SwaggerDefinition(
        info = @Info(description = "Drivers API", version = "1.0.0", title = "Drivers API"),
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
        schemes = { SwaggerDefinition.Scheme.HTTPS}
)
@Api(tags = "Drivers API Service", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, protocols = "https")
@RequestMapping("/driver")
public interface DriversEndpoint {

    @ApiOperation(value = "Создание новой карточки водителя")
    @RequestMapping(
            value = "/create",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            method = RequestMethod.POST
    )
    GeneralResponse<Integer> createDriverCard(@Valid @NotNull GeneralRequest<DriverParams> request);

    @ApiOperation(value = "Активация/деактивация карточки водителя")
    @RequestMapping(
            value = "/{option}",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            method = RequestMethod.POST
    )
    GeneralResponse<Void> statusDriverCard(
            @PathVariable("option") @NotBlank @Pattern(regexp = "activate|deactivate") String option,
            @Valid @NotNull GeneralRequest<DriverIDParam> request);
}
