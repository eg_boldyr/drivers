package com.drivers.api.response.parameters;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.StringJoiner;

@ApiModel(
        value = "Driver ID Parameter",
        description = "Уникальны идентефикатор карточки водителя"
)
public class DriverIDParam {

    @Valid
    @NotNull
    @Positive
    @ApiModelProperty(name = "driverId", notes = "ID-карты водителя", example = "785")
    private Integer driverId;

    public DriverIDParam() {}
    public DriverIDParam(Integer driverId) {
        this.driverId = driverId;
    }

    public Integer getDriverId() {
        return driverId;
    }
    public void setDriverId(Integer driverId) {
        this.driverId = driverId;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", DriverIDParam.class.getSimpleName() + "[", "]")
                .add("driverId=" + driverId)
                .toString();
    }
}
