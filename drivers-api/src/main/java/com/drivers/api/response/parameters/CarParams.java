package com.drivers.api.response.parameters;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.util.StringJoiner;

@ApiModel(
        value = "Car Parameters",
        description = "Базовый набор параметров для описания авто водителя"
)
public class CarParams {

    @Valid
    @NotNull
    @Size(max = 50)
    @ApiModelProperty(name = "model", notes = "Модель автомобиля", example = "Hundai Getz")
    private String model;

    @Valid
    @NotNull
    @Size(max = 50)
    @ApiModelProperty(name = "number", notes = "Номерной знак", example = "AA0012AM")
    private String number;

    @Valid
    @NotNull
    @Size(max = 50)
    @ApiModelProperty(name = "color", notes = "Цвет", example = "Черный")
    private String color;

    @Valid
    @NotNull
    @Positive
    @ApiModelProperty(name = "year", notes = "Год выпуска", example = "2015")
    private Integer year;

    public CarParams() {}
    public CarParams(String model, String number, String color, Integer year) {
        this.model = model;
        this.number = number;
        this.color = color;
        this.year = year;
    }

    public String getModel() {
        return model;
    }
    public void setModel(String model) {
        this.model = model;
    }
    public String getNumber() {
        return number;
    }
    public void setNumber(String number) {
        this.number = number;
    }
    public String getColor() {
        return color;
    }
    public void setColor(String color) {
        this.color = color;
    }
    public Integer getYear() {
        return year;
    }
    public void setYear(Integer year) {
        this.year = year;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", CarParams.class.getSimpleName() + "[", "]")
                .add("model='" + model + "'")
                .add("number='" + number + "'")
                .add("color='" + color + "'")
                .add("year=" + year)
                .toString();
    }
}
