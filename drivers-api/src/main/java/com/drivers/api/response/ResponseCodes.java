package com.drivers.api.response;

public final class ResponseCodes {

    public static final Long OK = 200L;
    public static final Long ERROR = 500L;
}
