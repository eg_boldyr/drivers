package com.drivers.api.response.parameters;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.util.StringJoiner;

@ApiModel(
        value = "Driver Parameters",
        description = "Базовый набор параметров для создания карты водителя"
)
public class DriverParams {

    @Valid
    @NotNull
    @Positive
    @ApiModelProperty(name = "phone", notes = "Номер телефона", example = "380501234567")
    private Long phone;

    @Valid
    @NotNull
    @Size(max = 50)
    @ApiModelProperty(name = "name", notes = "Имя водителя", example = "Иванов Иван")
    private String name;

    @Valid
    @NotNull
    @ApiModelProperty(name = "car", notes = "Автомобиль")
    private CarParams car;

    public DriverParams() {}
    public DriverParams(Long phone, String name, CarParams car) {
        this.phone = phone;
        this.name = name;
        this.car = car;
    }

    public Long getPhone() {
        return phone;
    }
    public void setPhone(Long phone) {
        this.phone = phone;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public CarParams getCar() {
        return car;
    }
    public void setCar(CarParams car) {
        this.car = car;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", DriverParams.class.getSimpleName() + "[", "]")
                .add("phone=" + phone)
                .add("name='" + name + "'")
                .add("car=" + car)
                .toString();
    }
}
