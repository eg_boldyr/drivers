package com.drivers.api.response;

import java.util.StringJoiner;

public class GeneralResponse<T> {

    private Long code;
    private String message;
    private T payload;

    public GeneralResponse() {}
    public GeneralResponse(Long code, String message, T payload) {
        this.code = code;
        this.message = message;
        this.payload = payload;
    }

    public Long getCode() {
        return code;
    }
    public String getMessage() {
        return message;
    }
    public T getPayload() {
        return payload;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", GeneralResponse.class.getSimpleName() + "[", "]")
                .add("code=" + code)
                .add("message='" + message + "'")
                .add("payload=" + payload)
                .toString();
    }
}
