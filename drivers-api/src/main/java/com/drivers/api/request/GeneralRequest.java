package com.drivers.api.request;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.StringJoiner;

public class GeneralRequest<T> {

    @Valid
    @NotNull
    private T parameters;

    public GeneralRequest() {}
    public GeneralRequest(T parameters) {
        this.parameters = parameters;
    }

    public T getParameters() {
        return parameters;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", GeneralRequest.class.getSimpleName() + "[", "]")
                .add("parameters=" + parameters)
                .toString();
    }
}
